# unarchive -- download original files from archive.org
# Copyright (C) 2008,2009,2016,2022 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only

DESTDIR = $(HOME)
prefix = .local
BINDIR = $(DESTDIR)/$(prefix)/bin
DATADIR = $(DESTDIR)/$(prefix)/share/unarchive
DOCDIR = $(DESTDIR)/$(prefix)/share/doc/unarchive
MANDIR = $(DESTDIR)/$(prefix)/share/man/man1

VERSION ?= $(shell test -d .git && git describe --always --dirty=+ || (cat share/unarchive/VERSION | head -n 1))
CODENAME ?= $(shell test -d .git && echo "Strawberry Jellyfish" || (cat share/unarchive/VERSION | tail -n+2 | head -n 1))
DATE ?= $(shell test -d .git && date --iso=s || (cat share/unarchive/VERSION | tail -n+3 | head -n 1))

all: var/www/unarchive/index.html var/www/unarchive/unarchive.1.html share/man/man1/unarchive.1.gz share/unarchive/VERSION

var/www/unarchive/index.html: var/www/unarchive/index.md
	pandoc var/www/unarchive/index.md --css "unarchive.css" --toc --standalone -o var/www/unarchive/index.html

var/www/unarchive/unarchive.1.html: share/man/man1/unarchive.1.md
	pandoc share/man/man1/unarchive.1.md --css "unarchive.css" --toc --standalone -o var/www/unarchive/unarchive.1.html

share/man/man1/unarchive.1.gz: share/man/man1/unarchive.1.md
	pandoc share/man/man1/unarchive.1.md --standalone -o share/man/man1/unarchive.1
	gzip -9 -f share/man/man1/unarchive.1

share/unarchive/VERSION:
	echo "$(VERSION)" > share/unarchive/VERSION
	echo "$(CODENAME)" >> share/unarchive/VERSION
	echo "$(DATE)" >> share/unarchive/VERSION
	touch -c -d '@0' share/unarchive/VERSION

release: all
	-rm -rf "unarchive-$(VERSION)"
	mkdir unarchive-$(VERSION)
	cat INDEX | cpio -pd "unarchive-$(VERSION)"
	tar --sort=name --mtime="$(DATE)" --owner=0 --group=0 --numeric-owner --pax-option=exthdr.name=%d/PaxHeaders/%f,delete=atime,delete=ctime --create --auto-compress --file "unarchive-$(VERSION).tar.xz" "unarchive-$(VERSION)"
	-gpg -u unarchive@mathr.co.uk -b unarchive-$(VERSION).tar.xz
	mkdir -p var/www/unarchive/download
	mv unarchive-$(VERSION).tar.xz* var/www/unarchive/download/

install: bin/unarchive bin/unarchive_collection share/unarchive/unarchive.xsl share/unarchive/VERSION share/man/man1/unarchive.1.gz share/man/man1/unarchive_collection.1.gz var/www/unarchive/index.html var/www/unarchive/unarchive.css
	install -d $(DATADIR)
	install -m 644 share/unarchive/unarchive.xsl $(DATADIR)
	install -m 644 share/unarchive/VERSION $(DATADIR)
	install -d $(MANDIR)
	install -m 644 share/man/man1/unarchive.1.gz $(MANDIR)
	install -m 644 share/man/man1/unarchive_collection.1.gz $(MANDIR)
	install -d $(DOCDIR)
	install -m 644 var/www/unarchive/index.html $(DOCDIR)
	install -m 644 var/www/unarchive/unarchive.css $(DOCDIR)
	install -d $(BINDIR)
	install -m 755 bin/unarchive $(BINDIR)/unarchive
	install -m 755 bin/unarchive_collection $(BINDIR)/unarchive_collection
	mandb

check:
	@echo "testing download"
	unarchive lab08
	@echo "verifying download (without network)"
	( cd lab08 && md5sum -c __unarchive.md5 )
	@echo "verifying download is up to date"
	unarchive lab08
	@echo "testing resume (missing file)"
	rm lab08/lab08_meta.xml
	unarchive lab08
	@echo "testing resume (broken file)"
	echo 'broken' > lab08/lab08_meta.xml
	-unarchive lab08
	rm lab08/lab08_meta.xml.~1~
	@echo "testing resume (partial file)"
	echo '<?xml version="1.0" encoding="UTF-8"?>' > lab08/lab08_meta.xml
	unarchive lab08
	rm lab08/lab08_meta.xml.~1~

.PHONY: all clean release check share/unarchive/VERSION
.SUFFIXES:
