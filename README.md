# unarchive

Download items and collections from the Internet Archive.

<https://mathr.co.uk/unarchive>

# documentation

[unarchive](var/www/unarchive/index.md)

# user manual

[unarchive(1)](share/man/man1/unarchive.1.md)

# legal

unarchive -- download from the Internet Archive

Copyright (C) 2008,2009,2016,2022  Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

---
<https://mathr.co.uk>
