---
title: unarchive
author: Claude Heiland-Allen
date: 2022-07-22
keywords: [internet, archive, download]
abstract: |
  Download from the Internet Archive.
...

# unarchive

Download items and collections from the Internet Archive.

<https://mathr.co.uk/unarchive>

# releases

current stable

:   [unarchive-1.0 "Strawberry Jellyfish" 2022-07-22](download/unarchive-1.0.tar.xz) ([sig](download/unarchive-1.0.tar.xz.sig))


# development

browse

:   <https://code.mathr.co.uk/unarchive>

download

:   `git clone https://code.mathr.co.uk/unarchive.git`

This version is likely to be experimental and/or broken at times.
Use at your own risk.

# security

Metadata downloaded from <https://archive.org> is implicitly trusted,
which may be a security risk, especially on untrusted networks.  It is
recommended to make backups and run with reduced privileges (e.g. using
a chroot jail).

Releases are signed with GPG:

key id

:   <mailto:unarchive@mathr.co.uk>

public key

:   <https://mathr.co.uk/unarchive/unarchive.pub>

fingerprint

:   `AA05 3AD6 6F47 CCA3 F71A  7A61 0271 249B 21EB 6A3B`

# install

dependencies

:   `sudo apt install bash coreutils cpio grep gzip sed tar wget xsltproc xz-utils`

    For rebuilding the documentation (which is included in the release
    bundles, but not in the development repository), also:

    `sudo apt install pandoc`

user

:   `make && make install`

global

:   `make && sudo make install DESTDIR=/ prefix=usr/local`

You can use `unarchive` without installing by writing the path to the
script, like `bin/unarchive` or `bin/unarchive_collection`, or adding
the full path to the `bin` directory to your `PATH` environment
variable.

# usage

local

:   `man unarchive`

online

:   [unarchive(1)](unarchive.1.html)

---
<https://mathr.co.uk>
