<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
# unarchive - download original files from archive.org
# Copyright (C) 2008,2009,2016,2022 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" />
<xsl:template match="/">
 <xsl:for-each select="files/file">
  <xsl:choose>
   <xsl:when test="@source='original' or @source='metadata'">
     <xsl:value-of select="md5" /><xsl:text> *</xsl:text><xsl:value-of select="@name" /><xsl:text>
</xsl:text>
   </xsl:when>
  </xsl:choose>
 </xsl:for-each>
</xsl:template>
</xsl:stylesheet>
